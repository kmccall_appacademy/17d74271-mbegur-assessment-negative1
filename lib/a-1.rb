# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  result = []
  nums.each_with_index do |int, idx|
    next if int + 1 == nums[idx+1]
    break if idx == nums.length - 1
    new_int = int + 1
    while new_int < nums[idx+1]
      result << new_int
      new_int += 1
    end
  end
  result
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  new_arr = binary.to_s.chars.reverse.map.with_index do |int, idx|
    int.to_i * (2**idx)
  end
  new_arr.reduce(:+)
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    hash = {}
    self.each do |k, v|
      if prc.call(k, v)
        hash[k] = v
      end
    end
    hash
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    new_hash = self
    hash.each do |k, v|
      if prc == nil
        new_hash[k] = hash[k]
      elsif self.keys.include?(k)
        new_hash[k] = prc.call(k, self[k], v)
      else
        new_hash[k] = hash[k]
      end
    end
    new_hash
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  positive_idx = [2, 1]
  negative_idx = [2, -1]
  if n > -1
    while positive_idx.length < n + 1
      positive_idx << positive_idx[-1] + positive_idx[-2]
    end
    return positive_idx[n]
  else
    while negative_idx.length < n.abs + 1
      negative_idx << negative_idx[-2] - negative_idx[-1]
    end
    return negative_idx.last
  end
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  result = []
  string.each_char.with_index do |ch1, idx1|
    string.each_char.with_index do |ch2, idx2|
      next if idx1 >= idx2
      if string[idx1..idx2] == string[idx1..idx2].reverse
      result << string[idx1..idx2]
      end
    end
  end
  if result.length == 0 || result.max_by {|word| word.length}.length < 3
    return false
  else
    return result.max_by {|word| word.length}.length
  end
end
